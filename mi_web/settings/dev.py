from .base import *

DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3', #define parametros de base de datos
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'), #es el parametro que requiere la base de datos
        
    }
}

SITE_URL="http://www.miweb.cl"
SITE_NAME="Mi web"
GOOGLE_API_KEY=' '
ALLOWED_HOSTS=["*"]
